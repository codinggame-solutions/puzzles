interface Enemy {
    name: string,
    distance: number
};

function fireClosestEnemy (enemy1: Enemy, enemy2: Enemy): string {
    return enemy1.distance < enemy2.distance ? enemy1.name : enemy2.name;
}

while (true) {
    const enemy1: Enemy = { name: readline(), distance: +readline() };
    const enemy2: Enemy = { name: readline(), distance: +readline() }; 
    
    console.log(fireClosestEnemy(enemy1, enemy2));
};