use std::io;

macro_rules! parse_input {
    ($x: expr, $t: ident) => ($x.trim().parse::<$t>().unwrap())
}

fn read_enemy_info() -> (String, i32) {
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    let name = input_line.trim().to_string();
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    let distance = parse_input!(input_line, i32);
    (name, distance)
}

fn main() {
    loop {
        let (enemy_1, dist_1) = read_enemy_info();
        let (enemy_2, dist_2) = read_enemy_info();

        println!("{}", match dist_1 < dist_2 { true => enemy_1, false => enemy_2 });
    }
}